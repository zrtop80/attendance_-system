from .designer.face import Ui_faceMainWindow
from .showclick import clickwindow
from .insightFace import insightFace
import sys  
import cv2  
import os
from PyQt5.QtWidgets import QApplication, QLabel, QPushButton, QVBoxLayout, QWidget ,QGraphicsView,QGraphicsScene ,QGraphicsPixmapItem
from PyQt5.QtGui import QPixmap, QImage  ,QFont,QPalette
from PyQt5.QtCore import QTimer, Qt ,QDateTime
from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtWidgets import QMainWindow


class FaceWindow(QMainWindow,Ui_faceMainWindow):
    def __init__(self,image_path):  #这里的image_path是
        super().__init__()
        self.flag = False
        self.image_path = image_path
        self.ui = Ui_faceMainWindow()  #实例化facewindow
        self.ui.setupUi(self) #设置UI
        #设置时间
        self.ui.dateTimeEdit.setDateTime(QDateTime.currentDateTime())
        #视频展示
        self.initUI()
        #label字体调整
        self.large_font = QFont("Arial", 48)  # 20是字体大小，"Arial"是字体名称  
        self.large_font.setBold(True)  # 设置为粗体  
        #self.large_font.setPixelSize(24)  # 另一种设置字体大小的方式，单位是像素  
        #self.large_font.setColor(Qt.red) #这个函数应该是被弃用了
        #点击按钮进行页面初始化
        self.ui.pushButton.clicked.connect(self.start_countdown)  # 连接按钮点击信号到 start_countdown 槽函数  
        self.timer = QTimer(self)  # 初始化 QTimer  
        self.timer.timeout.connect(self.update_countdown)  # 连接 QTimer 的 timeout 信号到 update_countdown 槽函数  
        self.countdown = 6  # 初始化倒计时值 
    #时间显示（在按下按钮后，倒数6秒进行采集信息）
        
    # 启动倒计时  
    def start_countdown(self):  
        self.ui.label_time.setFont(self.large_font)  # 设置label的字体
        palette = self.ui.label_time.palette()  
        palette.setColor(QPalette.WindowText, Qt.red)  # 设置文本颜色为红色  
        self.ui.label_time.setPalette(palette)  
        if self.countdown > 0:  
            if self.ui.plainTextEdit.toPlainText():
                self.timer.start(1000)  # 每秒触发一次 timeout 信号  
                self.update_countdown()  # 立即更新一次显示  
            else:
                QMessageBox.information(self,'提示',"请先输入工号")
        self.countdown = 6
  
    # 更新倒计时显示  
    def update_countdown(self):
        self.countdown -= 1  
        self.ui.label_time.setText(str(self.countdown))  # 假设 label_time 是 Ui_faceMainWindow 中的 QLabel  
        if self.countdown <= 0:  
            self.timer.stop()  # 倒计时结束，停止计时器  
            # 开始采集信息
            self.getImage() #目前这里是将图片保存下来了，后面需要修改，不需要保存，只有注册的时候需要保存
       
    #获取一张图片，用于注册        
    def getImage(self):
         # 捕获当前帧  
        ret, frame = self.cap.read()  
        if ret:  
            # 将帧保存为文件，例如 'capture.jpg'  
            file_path = os.path.join(os.getcwd(),self.image_path, self.ui.plainTextEdit.toPlainText()+'.jpg')  # 获取当前工作目录并创建文件路径  
            cv2.imwrite(file_path, cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY))  # 保存为RGB图片，因为OpenCV默认是BGR  cv2.COLOR_BGR2RGB
            print(f"Image saved to {file_path}")
            if self.image_path == 'login':
                a=insightFace()
                msg = a.picture_quality(file_path)
                if msg:
                    QMessageBox.information(self,"提示","你已经成功采集信息")
                    new_msg = a.face_search(file_path) #比对照片
                    if new_msg:
                        QMessageBox.information(self,"提示",'从人像库中找到了你的信息')
                        self.click_window = clickwindow(self.ui.plainTextEdit.toPlainText()) #将工号传入
                        self.click_window.show()
                        self.close()
                    else:
                        QMessageBox.information(self,"提示","未从信息库中找到你的信息，请先去注册")
                        self.close()
                else:
                    QMessageBox.information(self,"提示","请将人脸显示在摄像头前")
            elif self.image_path == 'register':
                a=insightFace()
                a.face_registration(file_path,'name') #人像库注册
                QMessageBox.information(self,"提示","你已经成功采集信息")
                self.close()
            else:
                self.close()
                QMessageBox.information(self,'提示',f'你的密码是:{self.image_path}')
            #self.close() #关闭界面
            #return file_path  #返回图片路径
        
            
    #摄像头采集
    def initUI(self):  
        self.scene = QGraphicsScene(self)  
        self.ui.graphicsView.setScene(self.scene)  
        # 初始化视频捕获和定时器  
        self.cap = cv2.VideoCapture(0)  
        self.timer = QTimer(self)  
        self.timer.timeout.connect(self.updateFrame)  
        self.timer.start(30)  # 每30毫秒更新一次帧  

    def updateFrame(self):  
        ret, frame = self.cap.read()  
        if ret:  
        # 加载人脸检测分类器（请确保XML文件的路径正确）  
            face_cascade = cv2.CascadeClassifier('/home/ddb/文档/自学编程内容/Attendance_System/logic/haarcascade_frontalface_default.xml')  
            if face_cascade.empty():  
                print("Error: Unable to load face cascade classifier XML file.")  
                return  
  
        # 将图像转换为灰度图，因为Haar级联分类器在灰度图上运行得更快  
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)  
  
        # 检测人脸  
            faces = face_cascade.detectMultiScale(gray, 1.3, 5)  
  
        # 在图像上绘制矩形框  
            for (x,y,w,h) in faces:  
                cv2.rectangle(frame, (x,y), (x+w,y+h), (255,0,0), 2)  
  
        # 转换带有矩形框的BGR图像为QImage  
            h, w, ch = frame.shape  # 使用frame的尺寸  
            bytes_per_line = ch * w  
            qt_image = QImage(frame.tobytes(), w, h, bytes_per_line, QImage.Format_RGB888).rgbSwapped()  
  
        # 转换为QPixmap并显示在QGraphicsView中  
            pixmap = QPixmap.fromImage(qt_image)  
            self.scene.clear()  # 清除之前的图形项  
            pixmap_item = QGraphicsPixmapItem(pixmap)  
            self.scene.addItem(pixmap_item)  
            self.ui.graphicsView.fitInView(self.scene.sceneRect(), Qt.KeepAspectRatio)  # 缩放视图以适应场景
    '''
    def updateFrame(self):
        ret, frame = self.cap.read()  
        if ret:  
            rgb_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)  
            h, w, ch = rgb_image.shape  
            bytes_per_line = ch * w  
            qt_image = QImage(rgb_image.data, w, h, bytes_per_line, QImage.Format_RGB888).rgbSwapped()  
            pixmap = QPixmap.fromImage(qt_image)  
            self.scene.clear()  # 清除之前的图形项  
            pixmap_item = QGraphicsPixmapItem(pixmap)  
            self.scene.addItem(pixmap_item)  
            self.ui.graphicsView.fitInView(self.scene.sceneRect(), Qt.KeepAspectRatio)  # 缩放视图以适应场景  
        '''
    #关闭事件响应
    def closeEvent(self, event):  
        self.cap.release()  
        self.timer.stop()  
        super().closeEvent(event) 