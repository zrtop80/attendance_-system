from PyQt5.QtWidgets import QMainWindow,QMessageBox
from .designer.click import Ui_clickMainWindow  #导入打卡图面
from .showadmin import adminshowwindow #需要使用adminshowwindow中的设置时间
from .location import get_location
from PyQt5.QtWidgets import QApplication, QLabel, QPushButton, QVBoxLayout, QWidget ,QGraphicsView,QGraphicsScene ,QGraphicsPixmapItem
from PyQt5.QtGui import QPixmap
from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
from .SQL.employees_attendance import EmployeeAttendance #打卡信息表
from datetime import datetime
import os

class clickwindow(QMainWindow):
    
    def __init__(self,employ_ID):
        self.morning_time = None  #类变量
        super().__init__()
        self.ui = Ui_clickMainWindow()
        self.ui.setupUi(self) #初始化UI
        self.show_picture()
        self.ui.pushButton_2.clicked.connect(self.on_work) #上班打卡
        self.ui.pushButton_3.clicked.connect(self.leave_work) #下班打卡
        #创建数据库对象
        self.db = EmployeeAttendance()
        self.employ_ID = employ_ID #工号
        
        
    

    #将登录的照片进行显示
    def show_picture(self):
        imagepath = '/home/ddb/文档/自学编程内容/Attendance_System/click.png' #得到照片路径
        # 假设你的QGraphicsView组件在UI中名为graphicsView  
        self.scene = QtWidgets.QGraphicsScene()  # 创建一个QGraphicsScene  
        # 加载图片  
        pixmap = QPixmap(imagepath)  
        '''
        self.scene.clear()  # 清除之前的图形项  
        pixmap_item = QGraphicsPixmapItem(pixmap)  
        self.scene.addItem(pixmap_item)  
        self.ui.graphicsView.fitInView(self.scene.sceneRect(), Qt.KeepAspectRatio)
        '''
        if not pixmap.isNull():  # 检查图片是否成功加载  
            item = QtWidgets.QGraphicsPixmapItem(pixmap)  # 创建一个QGraphicsPixmapItem  
            self.scene.addItem(item)  # 将项添加到场景中  
        self.ui.graphicsView.setScene(self.scene)   # 将场景设置到QGraphicsView上 
    
    #设置上班打卡
    def on_work(self):
        admin = adminshowwindow()
        result=admin.on_pushbutton_time()  #获得时间
        minute = int(result['早上上班时间'].hour()*60 + result['早上上班时间'].minute()) #将其转为分钟
        # 获取当前时间  
        now = datetime.now() 
        # 获取当前小时和分钟  
        hours = now.hour  
        minutes = now.minute  
        # 从午夜开始算，得到当前时间的分钟数  
        minutes_since_midnight = hours * 60 + minutes
        #将早上的分钟数作为早上的上班时间
        self.set_morningtime(int(minutes_since_midnight))
        if int(minutes_since_midnight) < int(minute):
            QMessageBox.information(self,"提示","打卡成功")
            #打卡成功就将数据写入数据库
            self.db.insert_punch_in_info(self.employ_ID,now,'否')
            self.close()
            #get_location() #得到位置

        else:
            data = int(minutes_since_midnight) - int(minute)
            if data < 15:
                QMessageBox.information(self,"提示","你迟到了{}分钟".format(data))
                self.db.insert_punch_in_info(self.employ_ID,now,'是')
                get_location() #获取位置
            else:
                QMessageBox.information(self,"提示","你已缺勤！") 
                #早上打卡缺勤，那么下午的时间即为当前时间
                self.db.insert_punch_out_info(self.employ_ID,now,'是',0)
                self.db.update_absent_status(self.employ_ID,'是') #判断是否缺勤
                self.close()
                #get_location() #获取位置
        #print(minutes)
        #return self.morning_time
        '''现在需要写入数据库，需要工号，工号是作为参数传进来的'''
    def set_morningtime(self,value):
        self.morning_time = value
    def get_morningtime(self):
        return self.morning_time
    #设置下班打卡
    def leave_work(self):
        admin = adminshowwindow()
        result=admin.on_pushbutton_time()  #获得时间
        minute = int((result['下午下班时间'].hour())*60 + result['下午下班时间'].minute()) #将其转为分钟
        # 获取当前时间  
        now = datetime.now()  
        print(now)
        print(minute)
        # 获取当前小时和分钟  
        hours = now.hour  
        minutes = now.minute  
        # 从午夜开始算，得到当前时间的分钟数  
        minutes_since_midnight = hours * 60 + minutes
        #计算工作时间
        if self.get_morningtime() is not None:
            date = (int(minutes_since_midnight) - self.get_morningtime()) / 60
        else:
            print("出错误了")
            date = 8

        if int(minutes_since_midnight) < int(minute):
            QMessageBox.information(self,"提示","未到下班时间，如果打卡算作缺勤")
            self.db.insert_punch_out_info(self.employ_ID,now,'是',date)
            self.db.update_absent_status(self.employ_ID,'是') #早退就算缺勤
            #get_location()#获取位置
        else:  #没有早退
            QMessageBox.information(self,'提示','恭喜你打卡完成！')
            self.db.insert_punch_out_info(self.employ_ID,now,'否',date)
            self.db.update_absent_status(self.employ_ID,'否') 
            self.close() #关闭界面
            #get_location() #位置
            
            
