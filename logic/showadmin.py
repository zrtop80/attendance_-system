from PyQt5.QtCore import Qt , QTime
from .designer.admin import Ui_adminMainWindow
from .designer.adminshow import Ui_adminShowMainWindow
from .SQL.administrator import adminDatabase
from .SQL.employees_attendance import EmployeeAttendance #员工打卡信息
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget ,QMessageBox ,QTableWidgetItem
import pandas as pd
#管理员显示界面
class adminshowwindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.ui = Ui_adminShowMainWindow() #实例化
        self.result = {'早上上班时间': None, '下午下班时间': None, '缺勤时间': 0, '迟到': 0}
        self.ui.setupUi(self)  #初始化adminwindow
        #设置时间默认值
        self.ui.timeEdit_morning.setTime(QTime(8,30))
        self.ui.timeEdit_afternoon.setTime(QTime(17,30))
        #设置限制默认值
        self.ui.spinBox_late.setValue(15)
        self.ui.spinBox_absent.setValue(30)
        self.ui.pushButton.clicked.connect(self.infor_Excel)
        self.admin_show() #显示管理员信息
        self.employees_show() #显示员工信息
        

    #用来获取设置值
    def on_pushbutton_time(self):
        #打印Excel表格
        #self.infor_Excel()
        #获取设置的上下班时间
        self.time_morning = self.ui.timeEdit_morning.time() #早上打卡时间
        self.time_afternoon = self.ui.timeEdit_afternoon.time() 
        self.result['早上上班时间'] = self.time_morning
        self.result['下午下班时间'] = self.time_afternoon
        
        #设置缺勤时间限制
        #设置一个范围
        self.ui.spinBox_absent.setRange(0,30) #最大30分钟
        self.absent = self.ui.spinBox_absent.value()
        self.result['缺勤时间'] = self.absent
        #迟到
        self.ui.spinBox_late.setRange(0,15) #最大15分钟
        self.late = self.ui.spinBox_late.value()
        self.result['迟到'] = self.late
        return self.result
    
    #将数据变为excel表格
    def infor_Excel(self):
        self.on_pushbutton_time() #时间设置获取
        db = EmployeeAttendance() #打卡信息
        db.connect_to_db() #连接
        results = db.traverse_employees() #遍历所有人的信息
        # 创建DataFrame  
        df = pd.DataFrame(results, columns=['ID', '工号', '姓名', '部门', '上班打卡时间', '是否迟到', '下班打卡时间', '是否早退', '工作时长', '是否缺勤'])  
        # 写入Excel文件  
        df.to_excel('Employess.xlsx', index=False)
        QMessageBox.information(self,'提示','表格已经生成！')
    #显示员工打卡信息（数据库暂时还未建立）
    def employees_show(self):
        db = EmployeeAttendance() #创建数据库实例
        db.connect_to_db() #连接数据库
        results = db.traverse_employees() #遍历所有人的信息
        self.ui.tableWidget.setRowCount(len(results))
        self.ui.tableWidget.setColumnCount(len(results[0])) #设置列数
        self.ui.tableWidget.setHorizontalHeaderLabels(['', '工号', '姓名','部门','上班打卡时间','是否迟到','下班打卡时间','是否早退','工作时长','是否缺勤']) #设置列显示
        for row in range(len(results)):
            for column in range(len(results[0])):
                item = QTableWidgetItem(f"{results[row][column]}")  
                self.ui.tableWidget.setColumnWidth(column,118) #设置列宽
                self.ui.tableWidget.setItem(row, column, item)


    #管理员信息显示
    def admin_show(self):
        db = adminDatabase() #创建一个数据库对象
        db.connect()
        results = db.select_all_employees() #查询所有管理员的信息
        self.ui.tableWidget_2.setRowCount(10) #设置行数 ，这里就写死了，因为管理者数量有限
        self.ui.tableWidget_2.setColumnCount(4) #设置列数
        self.ui.tableWidget_2.setHorizontalHeaderLabels(['', '工号', '姓名','密码']) #设置列显示
        #self.tableWidget.setVerticalHeaderLabels(['员工', '行2', '行3', '行4'])#设置行
        for row in range(len(results)):
            for column in range(len(results[0])):
                item = QTableWidgetItem(f"{results[row][column]}")  
                self.ui.tableWidget_2.setColumnWidth(column,300) #设置列宽
                self.ui.tableWidget_2.setItem(row, column, item)



#管理员登录界面
class adminwindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.ui = Ui_adminMainWindow()
        self.ui.setupUi(self)  # 初始化 UI
        self.ui.pushButton.clicked.connect(self.on_pushButton_login) #登录按钮
    #登录按钮
    def on_pushButton_login(self):
        db = adminDatabase() #创建一个数据库对象
        db.connect()
        #事实上，不需要创建表，这只用来测试
        
        self.admin_ID = self.ui.plainTextEdit_ID.toPlainText()
        self.admin_username = self.ui.plainTextEdit_username.toPlainText()
        self.admin_password = self.ui.plainTextEdit_password.toPlainText()
        #调用数据库进行查询
        result = db.select_employee_by_id(self.admin_ID) #根据工号进行查询
        if result[2]==self.admin_username and result[3]==self.admin_password:
            QMessageBox.information(self,"提示","欢迎登录！")
            #跳转业面
            self.adminWindow = adminshowwindow()
            self.adminWindow.show()
            self.close() #关闭此页面
        elif result[2] != self.admin_username:
            QMessageBox.information(self,"提示","用户名错误!")
        elif result[3] != self.admin_password:
            QMessageBox.information(self,"提示","密码错误!")
        db.close() #关闭数据库的连接

            