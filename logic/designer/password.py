# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'password.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(363, 559)
        MainWindow.setStyleSheet("background-image: url(:/images/images/three.jpeg);")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(90, 40, 191, 81))
        self.label.setStyleSheet("font: 36pt \"AR PL UKai CN\";\n"
"text-decoration: underline;")
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(10, 170, 51, 41))
        self.label_2.setObjectName("label_2")
        self.plainTextEdit = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit.setGeometry(QtCore.QRect(90, 170, 231, 41))
        self.plainTextEdit.setStyleSheet("background-color: rgba(255, 255, 255,70);")
        self.plainTextEdit.setObjectName("plainTextEdit")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(10, 240, 71, 41))
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(10, 320, 81, 41))
        self.label_4.setObjectName("label_4")
        self.plainTextEdit_2 = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit_2.setGeometry(QtCore.QRect(90, 240, 231, 41))
        self.plainTextEdit_2.setStyleSheet("background-color: rgba(255, 255, 255,70);")
        self.plainTextEdit_2.setObjectName("plainTextEdit_2")
        self.plainTextEdit_3 = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit_3.setGeometry(QtCore.QRect(90, 320, 231, 41))
        self.plainTextEdit_3.setStyleSheet("background-color: rgba(255, 255, 255,70);")
        self.plainTextEdit_3.setObjectName("plainTextEdit_3")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(130, 390, 100, 41))
        self.pushButton.setStyleSheet("QPushButton {  \n"
"    background-color: transparent;  \n"
"    border: none; /* 移除边框 */  \n"
"    outline: none; /* 移除可能的轮廓线 */  \n"
"}  \n"
"  \n"
"QPushButton:hover {  \n"
"   /* 悬停时字体变蓝色 */  \n"
"    color: rgb(85, 255, 255);\n"
"}  \n"
"  \n"
"QPushButton:pressed {  \n"
"    color: blue; /* 点击时也变蓝色 */  \n"
"}")
        self.pushButton.setObjectName("pushButton")
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(10, 10, 100, 27))
        self.pushButton_2.setStyleSheet("QPushButton {  \n"
"    background-color: transparent;  \n"
"    border: none; /* 移除边框 */  \n"
"    outline: none; /* 移除可能的轮廓线 */  \n"
"}  \n"
"  \n"
"QPushButton:hover {  \n"
"   /* 悬停时字体变蓝色 */  \n"
"    color: rgb(85, 255, 255);\n"
"}  \n"
"  \n"
"QPushButton:pressed {  \n"
"    color: blue; /* 点击时也变蓝色 */  \n"
"}")
        self.pushButton_2.setObjectName("pushButton_2")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "找回密码"))
        self.label_2.setText(_translate("MainWindow", "工号："))
        self.label_3.setText(_translate("MainWindow", "新密码："))
        self.label_4.setText(_translate("MainWindow", "再次输入："))
        self.pushButton.setText(_translate("MainWindow", "人脸比对"))
        self.pushButton_2.setText(_translate("MainWindow", "返回"))
import resources_rc
