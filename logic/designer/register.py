# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'register.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_registerMainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(465, 578)
        MainWindow.setStyleSheet("background-image: url(:/images/images/register.jpg);")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(170, 0, 111, 61))
        self.label.setStyleSheet("font: 36pt \"AR PL UKai TW\";\n"
"text-decoration: underline;")
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(60, 110, 101, 41))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(60, 200, 101, 41))
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(60, 280, 101, 41))
        self.label_4.setObjectName("label_4")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(60, 370, 101, 41))
        self.label_5.setObjectName("label_5")
        self.plainTextEdit = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit.setGeometry(QtCore.QRect(190, 110, 251, 51))
        self.plainTextEdit.setStyleSheet("background-color: rgba(255, 255, 255,70);")
        self.plainTextEdit.setObjectName("plainTextEdit")
        self.plainTextEdit_2 = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit_2.setGeometry(QtCore.QRect(190, 200, 251, 51))
        self.plainTextEdit_2.setStyleSheet("background-color: rgba(255, 255, 255,70);")
        self.plainTextEdit_2.setObjectName("plainTextEdit_2")
        self.plainTextEdit_3 = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit_3.setGeometry(QtCore.QRect(190, 270, 251, 51))
        self.plainTextEdit_3.setStyleSheet("background-color: rgba(255, 255, 255,70);")
        self.plainTextEdit_3.setObjectName("plainTextEdit_3")
        self.plainTextEdit_4 = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit_4.setGeometry(QtCore.QRect(190, 360, 251, 51))
        self.plainTextEdit_4.setStyleSheet("background-color: rgba(255, 255, 255,70);")
        self.plainTextEdit_4.setObjectName("plainTextEdit_4")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(10, 10, 100, 27))
        self.pushButton.setStyleSheet("QPushButton {  \n"
"    background-color: transparent;  \n"
"    border: none; /* 移除边框 */  \n"
"    outline: none; /* 移除可能的轮廓线 */  \n"
"}  \n"
"  \n"
"QPushButton:hover {  \n"
"    color: blue; /* 悬停时字体变蓝色 */  \n"
"}  \n"
"  \n"
"QPushButton:pressed {  \n"
"    color: blue; /* 点击时也变蓝色 */  \n"
"}")
        self.pushButton.setObjectName("pushButton")
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(60, 450, 161, 41))
        self.pushButton_2.setStyleSheet("QPushButton {  \n"
"    background-color: transparent;  \n"
"    border: none; /* 移除边框 */  \n"
"    outline: none; /* 移除可能的轮廓线 */  \n"
"}  \n"
"  \n"
"QPushButton:hover {  \n"
"    /* 悬停时字体变蓝色 */  \n"
"    color: rgb(85, 255, 255);\n"
"}  \n"
"  \n"
"QPushButton:pressed {  \n"
"    color: blue; /* 点击时也变蓝色 */  \n"
"}")
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setGeometry(QtCore.QRect(270, 450, 161, 41))
        self.pushButton_3.setStyleSheet("QPushButton {  \n"
"    background-color: transparent;  \n"
"    border: none; /* 移除边框 */  \n"
"    outline: none; /* 移除可能的轮廓线 */  \n"
"}  \n"
"  \n"
"QPushButton:hover {  \n"
"    color: blue; /* 悬停时字体变蓝色 */  \n"
"}  \n"
"  \n"
"QPushButton:pressed {  \n"
"    color: blue; /* 点击时也变蓝色 */  \n"
"}")
        self.pushButton_3.setObjectName("pushButton_3")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "注册"))
        self.label_2.setText(_translate("MainWindow", "工号："))
        self.label_3.setText(_translate("MainWindow", "姓名："))
        self.label_4.setText(_translate("MainWindow", "部门："))
        self.label_5.setText(_translate("MainWindow", "密码："))
        self.pushButton.setText(_translate("MainWindow", "返回登录"))
        self.pushButton_2.setText(_translate("MainWindow", "人像采集"))
        self.pushButton_3.setText(_translate("MainWindow", "确认"))
import resources_rc
