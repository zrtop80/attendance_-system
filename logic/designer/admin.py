# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'admin.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_adminMainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(381, 503)
        MainWindow.setStyleSheet("background-image: url(:/images/images/register.jpg);")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(70, 40, 241, 71))
        self.label.setStyleSheet("font: 36pt \"AR PL UKai HK\";\n"
"text-decoration: underline;")
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(30, 160, 79, 31))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(30, 240, 79, 31))
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(30, 320, 79, 31))
        self.label_4.setObjectName("label_4")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(130, 430, 141, 51))
        self.pushButton.setStyleSheet("QPushButton {  \n"
"    background-color: transparent;  \n"
"    border: none; /* 移除边框 */  \n"
"    outline: none; /* 移除可能的轮廓线 */  \n"
"}  \n"
"  \n"
"QPushButton:hover {  \n"
"    color: blue; /* 悬停时字体变蓝色 */  \n"
"}  \n"
"  \n"
"QPushButton:pressed {  \n"
"    color: blue; /* 点击时也变蓝色 */  \n"
"}")
        self.pushButton.setObjectName("pushButton")
        self.plainTextEdit_ID = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit_ID.setGeometry(QtCore.QRect(120, 150, 241, 51))
        self.plainTextEdit_ID.setObjectName("plainTextEdit_ID")
        self.plainTextEdit_username = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit_username.setGeometry(QtCore.QRect(120, 220, 241, 51))
        self.plainTextEdit_username.setObjectName("plainTextEdit_username")
        self.plainTextEdit_password = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit_password.setGeometry(QtCore.QRect(120, 300, 241, 51))
        self.plainTextEdit_password.setObjectName("plainTextEdit_password")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "管理员登录"))
        self.label_2.setText(_translate("MainWindow", "工号："))
        self.label_3.setText(_translate("MainWindow", "姓名："))
        self.label_4.setText(_translate("MainWindow", "密码："))
        self.pushButton.setText(_translate("MainWindow", "确认"))
import resources_rc
