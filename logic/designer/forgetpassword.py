# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/ddb/文档/自学编程内容/Attendance_System/form/forgetpassword.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_forgetMainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(382, 566)
        MainWindow.setStyleSheet("background-image: url(:/images/images/five.jpg);")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(90, 30, 211, 71))
        self.label.setStyleSheet("color: rgb(85, 85, 255);\n"
"text-decoration: underline;\n"
"font: 36pt \"AR PL UKai CN\";")
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(30, 160, 61, 41))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(30, 240, 61, 41))
        self.label_3.setObjectName("label_3")
        self.plainTextEdit = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit.setGeometry(QtCore.QRect(110, 160, 261, 51))
        self.plainTextEdit.setStyleSheet("background-color: rgba(255, 255, 255,70);")
        self.plainTextEdit.setObjectName("plainTextEdit")
        self.plainTextEdit_2 = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit_2.setGeometry(QtCore.QRect(110, 230, 261, 51))
        self.plainTextEdit_2.setStyleSheet("background-color: rgba(255, 255, 255,70);")
        self.plainTextEdit_2.setObjectName("plainTextEdit_2")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(90, 330, 231, 51))
        self.pushButton.setStyleSheet("QPushButton {  \n"
"    background-color: transparent;  \n"
"    border: none; /* 移除边框 */  \n"
"    outline: none; /* 移除可能的轮廓线 */  \n"
"}  \n"
"  \n"
"QPushButton:hover {  \n"
"     \n"
"    color: rgb(85, 255, 255);\n"
"    \n"
"    font: 24pt \"AR PL UKai CN\";\n"
"    text-decoration: underline;\n"
"}  \n"
"  \n"
"QPushButton:pressed {  \n"
"    color: blue; /* 点击时也变蓝色 */  \n"
"}")
        self.pushButton.setObjectName("pushButton")
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(219, 510, 161, 51))
        self.pushButton_2.setStyleSheet("QPushButton {  \n"
"    background-color: transparent;  \n"
"    border: none; /* 移除边框 */  \n"
"    outline: none; /* 移除可能的轮廓线 */  \n"
"}  \n"
"  \n"
"QPushButton:hover {  \n"
"     \n"
"    color: rgb(85, 255, 255);\n"
"    \n"
"    font: 24pt \"AR PL UKai CN\";\n"
"    text-decoration: underline;\n"
"}  \n"
"  \n"
"QPushButton:pressed {  \n"
"    color: blue; /* 点击时也变蓝色 */  \n"
"}")
        self.pushButton_2.setObjectName("pushButton_2")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "忘记密码"))
        self.label_2.setText(_translate("MainWindow", "姓名："))
        self.label_3.setText(_translate("MainWindow", "工号："))
        self.pushButton.setText(_translate("MainWindow", "人脸验证"))
        self.pushButton_2.setText(_translate("MainWindow", "返回登录"))
import resources_rc
