# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'adminshow.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_adminShowMainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1071, 815)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(10, 30, 111, 19))
        self.label.setObjectName("label")
        self.timeEdit_morning = QtWidgets.QTimeEdit(self.centralwidget)
        self.timeEdit_morning.setGeometry(QtCore.QRect(130, 30, 118, 28))
        self.timeEdit_morning.setObjectName("timeEdit_morning")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(280, 30, 111, 19))
        self.label_2.setObjectName("label_2")
        self.timeEdit_afternoon = QtWidgets.QTimeEdit(self.centralwidget)
        self.timeEdit_afternoon.setGeometry(QtCore.QRect(400, 30, 118, 28))
        self.timeEdit_afternoon.setObjectName("timeEdit_afternoon")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(750, 30, 61, 19))
        self.label_3.setObjectName("label_3")
        self.spinBox_absent = QtWidgets.QSpinBox(self.centralwidget)
        self.spinBox_absent.setGeometry(QtCore.QRect(820, 30, 71, 28))
        self.spinBox_absent.setObjectName("spinBox_absent")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(560, 30, 61, 19))
        self.label_4.setObjectName("label_4")
        self.spinBox_late = QtWidgets.QSpinBox(self.centralwidget)
        self.spinBox_late.setGeometry(QtCore.QRect(620, 30, 71, 28))
        self.spinBox_late.setObjectName("spinBox_late")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(909, 30, 131, 31))
        self.pushButton.setStyleSheet("QPushButton {  \n"
"    background-color: transparent;  \n"
"    border: none; /* 移除边框 */  \n"
"    outline: none; /* 移除可能的轮廓线 */  \n"
"}  \n"
"  \n"
"QPushButton:hover {  \n"
"    /* 悬停时字体变蓝色 */  \n"
"    color: rgb(85, 255, 255);\n"
"    font: 22pt \"AR PL UKai CN\";\n"
"    text-decoration: underline;\n"
"}  \n"
"  \n"
"QPushButton:pressed {  \n"
"    color: blue; /* 点击时也变蓝色 */  \n"
"}")
        self.pushButton.setObjectName("pushButton")
        self.tableWidget = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget.setGeometry(QtCore.QRect(10, 120, 1051, 361))
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)
        self.tableWidget_2 = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget_2.setGeometry(QtCore.QRect(10, 550, 1051, 261))
        self.tableWidget_2.setObjectName("tableWidget_2")
        self.tableWidget_2.setColumnCount(0)
        self.tableWidget_2.setRowCount(0)
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(10, 80, 131, 31))
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setGeometry(QtCore.QRect(10, 510, 131, 31))
        self.label_6.setObjectName("label_6")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "上班打卡时间："))
        self.label_2.setText(_translate("MainWindow", "下班打卡时间："))
        self.label_3.setText(_translate("MainWindow", "缺勤："))
        self.label_4.setText(_translate("MainWindow", "迟到："))
        self.pushButton.setText(_translate("MainWindow", "信息导出"))
        self.label_5.setText(_translate("MainWindow", "员工打卡信息"))
        self.label_6.setText(_translate("MainWindow", "管理员信息"))
