# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'login.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_LoginMainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(478, 661)
        MainWindow.setStyleSheet("background-image: url(:/images/images/background.jpeg);")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(150, 40, 201, 71))
        self.label.setStyleSheet("font: 36pt \"AR PL UKai TW\";\n"
"background-color: rgba(255, 255, 255,0);\n"
"text-decoration: underline;")
        self.label.setObjectName("label")
        self.pushButton_admin = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_admin.setGeometry(QtCore.QRect(10, 20, 91, 31))
        self.pushButton_admin.setStyleSheet("QPushButton {  \n"
"    background-color: transparent;  \n"
"    border: none; /* 移除边框 */  \n"
"    outline: none; /* 移除可能的轮廓线 */  \n"
"}  \n"
"  \n"
"QPushButton:hover {  \n"
"    /* 悬停时字体变蓝色 */  \n"
"    color: rgb(85, 255, 255);\n"
"}  \n"
"  \n"
"QPushButton:pressed {  \n"
"    color: blue; /* 点击时也变蓝色 */  \n"
"}")
        self.pushButton_admin.setObjectName("pushButton_admin")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(40, 160, 71, 21))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(40, 240, 61, 21))
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(40, 310, 61, 21))
        self.label_4.setObjectName("label_4")
        self.plainTextEdit = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit.setGeometry(QtCore.QRect(120, 160, 311, 41))
        self.plainTextEdit.setStyleSheet("background-color: rgba(255, 255, 255,70);")
        self.plainTextEdit.setObjectName("plainTextEdit")
        self.plainTextEdit_2 = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit_2.setGeometry(QtCore.QRect(120, 230, 311, 41))
        self.plainTextEdit_2.setStyleSheet("background-color: rgba(255, 255, 255,70);")
        self.plainTextEdit_2.setObjectName("plainTextEdit_2")
        self.plainTextEdit_3 = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit_3.setGeometry(QtCore.QRect(120, 300, 311, 41))
        self.plainTextEdit_3.setStyleSheet("background-color: rgba(255, 255, 255,70);")
        self.plainTextEdit_3.setObjectName("plainTextEdit_3")
        self.pushButton_face = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_face.setGeometry(QtCore.QRect(220, 370, 111, 41))
        self.pushButton_face.setStyleSheet("QPushButton {  \n"
"    background-color: transparent;  \n"
"    border: none; /* 移除边框 */  \n"
"    outline: none; /* 移除可能的轮廓线 */  \n"
"}  \n"
"  \n"
"QPushButton:hover {  \n"
"    /* 悬停时字体变蓝色 */  \n"
"    color: rgb(85, 255, 255);\n"
"}  \n"
"  \n"
"QPushButton:pressed {  \n"
"    color: blue; /* 点击时也变蓝色 */  \n"
"}")
        self.pushButton_face.setObjectName("pushButton_face")
        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setGeometry(QtCore.QRect(220, 440, 111, 41))
        self.pushButton_3.setStyleSheet("QPushButton {  \n"
"    background-color: transparent;  \n"
"    border: none; /* 移除边框 */  \n"
"    outline: none; /* 移除可能的轮廓线 */  \n"
"}  \n"
"  \n"
"QPushButton:hover {  \n"
"    /* 悬停时字体变蓝色 */  \n"
"    color: rgb(85, 255, 255);\n"
"}  \n"
"  \n"
"QPushButton:pressed {  \n"
"    color: blue; /* 点击时也变蓝色 */  \n"
"}")
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_register = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_register.setGeometry(QtCore.QRect(330, 30, 131, 31))
        self.pushButton_register.setStyleSheet("QPushButton {  \n"
"    background-color: transparent;  \n"
"    border: none; /* 移除边框 */  \n"
"    outline: none; /* 移除可能的轮廓线 */  \n"
"}  \n"
"  \n"
"QPushButton:hover {  \n"
"    /* 悬停时字体变蓝色 */  \n"
"    color: rgb(85, 255, 255);\n"
"}  \n"
"  \n"
"QPushButton:pressed {  \n"
"    color: blue; /* 点击时也变蓝色 */  \n"
"}")
        self.pushButton_register.setObjectName("pushButton_register")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(370, 620, 100, 27))
        self.pushButton.setStyleSheet("QPushButton {  \n"
"    background-color: transparent;  \n"
"    border: none; /* 移除边框 */  \n"
"    outline: none; /* 移除可能的轮廓线 */  \n"
"}  \n"
"  \n"
"QPushButton:hover {  \n"
"    /* 悬停时字体变蓝色 */  \n"
"    color: rgb(85, 255, 255);\n"
"}  \n"
"  \n"
"QPushButton:pressed {  \n"
"    color: blue; /* 点击时也变蓝色 */  \n"
"}")
        self.pushButton.setObjectName("pushButton")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "欢迎登录"))
        self.pushButton_admin.setText(_translate("MainWindow", "管理员"))
        self.label_2.setText(_translate("MainWindow", "工号："))
        self.label_3.setText(_translate("MainWindow", "姓名："))
        self.label_4.setText(_translate("MainWindow", "密码："))
        self.pushButton_face.setText(_translate("MainWindow", "人脸识别登录"))
        self.pushButton_3.setText(_translate("MainWindow", "登录"))
        self.pushButton_register.setText(_translate("MainWindow", "注册"))
        self.pushButton.setText(_translate("MainWindow", "忘记密码"))
import resources_rc
