from .designer.forgetpassword import Ui_forgetMainWindow #导入界面ui
from .showFaceView import FaceWindow #人脸识别
from .SQL.employess import EmployeeDatabase
from PyQt5.QtWidgets import QMainWindow, QMessageBox
class forgetwindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.ui = Ui_forgetMainWindow() #实例对象
        self.ui.setupUi(self)
        self.ui.pushButton.clicked.connect(self.judget_message)
        self.ui.pushButton_2.clicked.connect(self.on_pushbutton_login)

    #判断工号和姓名是否输入，以及是否正确
    def judget_message(self):
        self.ID = self.ui.plainTextEdit_2.toPlainText() #获取工号
        self.username = self.ui.plainTextEdit.toPlainText() #获取用户名
        if self.ID and self.username:
            if self.select_information(): #进行数据查找
                self.face_view()
                #QMessageBox.information(self,'提示',f'你的密码：{self.password}')
        else:
            QMessageBox.information(self,'提示','请录入信息')
                
    #数据库数据查询
    def select_information(self):
        db = EmployeeDatabase()
        db.connect() #连接数据库
        results = db.find_by_id(self.ID)
        if results is None:
            QMessageBox.information(self,'提示','未查询到你的任何信息')
            self.ui.plainTextEdit.clear()
            self.ui.plainTextEdit_2.clear()
            return False
        else:
            results = db.find_by_id(self.ID) #根据工号查找数据，需要比对姓名
            if self.username == results[2]:
                #得到密码
                self.password = results[4]
                return True
            else:
                QMessageBox.information(self,'提示','你的姓名不正确！')
                self.ui.plainTextEdit_2.clear()
                return False
    #进行人脸识别
    def face_view(self):
        self.facewindow =FaceWindow(self.password)
        self.facewindow.show() #进行人脸识别
        
    def on_pushbutton_login(self):
         self.close() #关闭



        
        

