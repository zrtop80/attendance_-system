from .designer.login import Ui_LoginMainWindow  #登录界面
from .showregister import registerWindow
from .showadmin import adminwindow
from .showFaceView import FaceWindow
from .showforget import forgetwindow
from .SQL.employess import EmployeeDatabase
from PyQt5.QtWidgets import QApplication, QMainWindow ,QMessageBox

class LoginMainWindow(QMainWindow):
    flag = False
    def __init__(self):  
        super().__init__()  
        #连接数据库
        self.db = EmployeeDatabase()
        self.db.connect() #连接数据库
        self.ui = Ui_LoginMainWindow()  #实例化login页面  
        self.ui.setupUi(self)  # 初始化 UI 
        # 连接信号和槽  
        self.ui.pushButton_register.clicked.connect(self.on_pushButton_register)   #注册按钮
        #管理员按钮
        self.ui.pushButton_admin.clicked.connect(self.on_pushButton_admin)
        #人脸信息采集按钮
        self.ui.pushButton_face.clicked.connect(self.on_pushButton_face)
        #登录按钮
        self.ui.pushButton_3.clicked.connect(self.on_pushbutton_login)
        #忘记密码
        self.ui.pushButton.clicked.connect(self.on_pushbutton_forget)
    
    #注册界面
    def on_pushButton_register(self):  
        # 实现按钮点击后的功能  
        print("Button clicked!")
        self.second_window = registerWindow()  
        self.second_window.show() 
    #管理员界面
    def on_pushButton_admin(self):
        self.admin_window = adminwindow()
        self.admin_window.show()
        self.close() #关闭主登录界面
    #人脸信息采集
    def on_pushButton_face(self):
        if self.flag:
            self.face_window = FaceWindow('login')
            self.face_window.show()
        #self.face_window.destroyed.connect(self.on_face_window_closed)  # 连接信号到槽函数，判断是否关闭
            self.close()
        else:
            QMessageBox.information(self,"提示",'请先登录！')
    #人脸信息比对
    def face_matching(self):
        pass
    #原本是用于显示人像图片的，但是这里就不使用了
    '''
    def on_face_window_closed(self):
            print("窗口被销毁")
            self.click_window = clickwindow()
            self.click_window.show()
    '''
    #登录按钮事件
    def on_pushbutton_login(self):
        self.ID = self.ui.plainTextEdit.toPlainText()  #工号
        self.username = self.ui.plainTextEdit_2.toPlainText()  #姓名
        self.password = self.ui.plainTextEdit_3.toPlainText()  #密码
        results = self.db.find_by_id(self.ID)
        if results:
            if self.username == results[2] and self.password == results[4]:
                QMessageBox.information(self,"提示",'信息正确，请进行人脸信息采集！')
                #self.db.close() #关闭数据库的连接
                self.flag = True
            elif self.username == results[2] and self.password != results[4]:
                QMessageBox.information(self,'提示','密码错误')
                self.ui.plainTextEdit_3.clear() #清除密码
                self.flag = False
            else:
                QMessageBox.information(self,'提示','姓名错误！')
                self.ui.plainTextEdit_2.clear() #清除姓名
                self.flag = False
        else:
            QMessageBox.information(self,'提示','工号不存在！')
            self.ui.plainTextEdit.clear()#清除
            self.flag = False
                
    #忘记密码事件
    def on_pushbutton_forget(self):
        self.forgetwindow = forgetwindow()
        self.forgetwindow.show()
        
        