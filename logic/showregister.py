from .designer.register import Ui_registerMainWindow
from PyQt5.QtWidgets import QApplication, QMainWindow ,QMessageBox
from .SQL.employess import EmployeeDatabase  #引入数据库
from .SQL.employees_attendance import EmployeeAttendance
from .showFaceView import FaceWindow
#注册界面
class registerWindow(QMainWindow):
    def __init__(self):  
        super().__init__()  
        self.flag = False
        self.ui = Ui_registerMainWindow()  #实例化login页面  
        self.ui.setupUi(self)  # 初始化 UI
        #self.setFixedSize(self.width, self.height)
        self.ui.pushButton_2.clicked.connect(self.on_pushButton_face)
        self.ui.pushButton_3.clicked.connect(self.write_sql)
        self.ui.pushButton.clicked.connect(self.on_pushbutton_login)
    #人脸识别跳转
    def on_pushButton_face(self):
        if self.flag:
            self.face_window = FaceWindow('register')
            self.face_window.show()
        else:
            QMessageBox.information(self,'提示','请先填写个人信息！')
        '''
        if self.file_path:
            return True
        else:
            return False
        '''
    #先实现数据库内容的写入
    def write_sql(self):
        self.ID = self.ui.plainTextEdit.toPlainText() #获取ID
        self.username = self.ui.plainTextEdit_2.toPlainText() #获取姓名
        self.department = self.ui.plainTextEdit_3.toPlainText() #获取部门
        self.password = self.ui.plainTextEdit_4.toPlainText() #获取密码
        self.ImagePath = '/home/ddb/文档/自学编程内容/Attendance_System/register' #这里的路径暂时不给出，需要根据后面的图片采集进行分析
        #开始写入数据库
        with EmployeeDatabase() as employees:
            employees.create_table()
        # 插入数据
            employees.insert(self.ID, self.username, self.department, self.password, self.ImagePath) #写如员工信息表
            #employees.close() #关闭数据库，这里不能关闭数据库，后面还要使用
        #现在要写入打卡表
        db = EmployeeAttendance()
        db.create_db()#创建数据库
        db.insert_employee_info(self.ID,self.username,self.department) #插入数据
        db.close_db_connection()#关闭数据库连接
        QMessageBox.information(self,'恭喜','你已完成注册，请开始人脸信息采集')
        self.flag = True

    def on_pushbutton_login(self):
        self.close()